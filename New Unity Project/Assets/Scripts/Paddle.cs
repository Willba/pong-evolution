using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
    
{
    private float speed;
    string input; 

    void Start()
    {
        speed = 20f;
        input = "P1";
    }

    // Update is called once per frame
    void Update()
    {
        float move = Input.GetAxis(input) * Time.deltaTime * speed;

        transform.Translate (move * Vector3.forward);
    }
}
