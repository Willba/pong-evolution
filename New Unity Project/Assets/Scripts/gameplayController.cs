﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameplayController : MonoBehaviour 
{
	private Transform P1;
	private Transform P2;
	private Transform P3;
	private Transform P4;

	private int score1 = 0;
	private int score2 = 0;
	private int score3 = 0;
	private int score4 = 0;

	public float maxTime = 120;
	public float sceneTime = 0;

	public Text score1Text;
	private string score1Output = "P1 Score: ";

	public Text score2Text;
	private string score2Output = "P2 Score: ";

	public Text score3Text;
	private string score3Output = "P3 Score: ";

	public Text score4Text;
	private string score4Output = "P4 Score: ";

	public Text timerText;
	private string timeOutput = "Timer: ";

	public GameObject ball;

	void Awake()
	{
		PlayerPrefs.SetInt("score", 0);

		score1Text.text = score1Output + score1;
		score2Text.text = score2Output + score2;
		score3Text.text = score3Output + score3;
		score4Text.text = score4Output + score4;

		P1 = GameObject.Find ("Paddle1").transform;
		P2 = GameObject.Find ("Paddle2").transform;
		P3 = GameObject.Find ("Paddle3").transform;
		P4 = GameObject.Find ("Paddle4").transform;
	}

	void Update () 
	{
		sceneTime += Time.deltaTime;
		UpdateTimer();
	}

	public void UpdateScore()
	{
		score1 ++;
		score2 ++;
		score3 ++;
		score4 ++;

		score1Text.text = score1Output + score1;
		score2Text.text = score2Output + score2;
		score3Text.text = score3Output + score3;
		score4Text.text = score4Output + score4;

		ball.transform.position = new Vector3(0, 5, 0);
		ball.GetComponent<Rigidbody>().velocity = -ball.GetComponent<Rigidbody>().velocity;

	}
    void UpdateTimer()
    {
        timerText.text = timeOutput + (Mathf.RoundToInt(maxTime - sceneTime)).ToString();
    }
    

}
