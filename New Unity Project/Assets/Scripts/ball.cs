using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour {
    float speed;
    float radius;
    Vector3 direction;

    void Start() {
        direction = Vector3.one.normalized;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate (direction * speed * Time.deltaTime);
    }
}
