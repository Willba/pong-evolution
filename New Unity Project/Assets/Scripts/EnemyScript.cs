﻿ using UnityEngine;
using System.Collections;

public class EnemyScript: MonoBehaviour 
{
	private Rigidbody body;
	public bool randomForce = true;
	public float velocity = 10f;

	public GameObject controller;
	private GameplayController gameController;
	// Use this for initialization
	void Awake () 
	{
		gameController = controller.GetComponent<GameplayController>();
		body = gameObject.GetComponent<Rigidbody>();
		body.AddForce(new Vector3(25, 0, 25), ForceMode.Impulse);
		
	}


}
